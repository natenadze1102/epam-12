import express from "express";
import "dotenv/config";
import bodyParser from "body-parser";
import routes from "./routes/index.js";
import UsersService from "./services/UsersService.js";
import TasksService from "./services/TasksService.js";
// import cookieParser from "cookie-parser";
import passport from "passport";
import "./services/AuthService.js";

import connectToDb from "./utils/db.js";

const app = express();
const PORT = process.env.PORT;

await connectToDb();
const usersService = new UsersService();
const tasksService = new TasksService();

// app.use(cookieParser());
// app.use(passport.initialize());
app.use(bodyParser.json());
app.use("/", routes(usersService, tasksService));

app.use((err, req, res, next) => {
  console.log(err);
  if (res.headersSent) {
    return next(err);
  }
  res
    .status(err.status || 500)
    .json({ message: err.message || "Something went wrong on the server." });
});

app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!");
});

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
