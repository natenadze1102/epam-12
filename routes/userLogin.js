import express from "express";
import { userSchema } from "../validationSchemas/UserSchema.js";

const loginRoute = express.Router();

export default (userAuth) => {
  loginRoute.post("/", async (req, res, next) => {
    try {
      const { error } = userSchema.validate(req.body);
      if (error) {
        return res.status(400).json({
          message: `Email field contains an invalid email, or any of the fields is missing, or a user with the provided credentials does not exist.`,
        });
      }
      const { user, token } = await userAuth(req.body.email, req.body.password);
      const responseUser = {
        user: {
          email: user.email,
        },
      };
      res.status(200).json({ responseUser, token });
    } catch (err) {
      err.status = 400;

      next(err);
    }
  });
  return loginRoute;
};
