import express from "express";
import {
  taskSchema,
  deleteTaskSchema,
} from "../validationSchemas/taskSchema.js";

export const createTaskRoute = (createTask) => {
  const tasksRoute = express.Router();
  tasksRoute.post("/", async (req, res, next) => {
    console.log("post createnewTasK");
    try {
      const newTask = {
        title: req.body.title,
        description: req.body.description,
      };

      await createTask(req.user, newTask);
      res
        .status(201)
        .json({ message: "A TODO task was successfully created for a user." });
    } catch (err) {
      next(err);
    }
  });
  return tasksRoute;
};

export const editTaskRoute = (editUserTask) => {
  const tasksRoute = express.Router();

  tasksRoute.put("/:title", async (req, res, next) => {
    console.log("is editing");

    try {
      const taskTitle = req.params.title;

      const { error } = taskSchema.validate(req.body);

      if (error) {
        return res.status(400).json({
          message: `Schema validation of the body failed, or a user already has a task with the provided title.`,
        });
      }

      const updatedTask = {
        title: req.body.title,
        description: req.body.description,
      };

      await editUserTask(req.user, updatedTask, taskTitle);
      res.status(200).json({ message: "A task was successfully updated." });
    } catch (err) {
      next(err);
    }
  });
  return tasksRoute;
};

export const getUserTasksRoute = (getUserTasks) => {
  const tasksRoute = express.Router();

  tasksRoute.get("/", async (req, res, next) => {
    try {
      const tasks = await getUserTasks(req.user);
      res.status(200).json(tasks);
    } catch (err) {
      next(err);
    }
  });
  return tasksRoute;
};

export const getUserTasksDoneRoute = (getUserTasksDone) => {
  const tasksRoute = express.Router();
  tasksRoute.get("/", async (req, res, next) => {
    try {
      const tasks = await getUserTasksDone(req.user);

      res.status(200).json(tasks);
    } catch (err) {
      next(err);
    }
  });
  return tasksRoute;
};

export const markUserTaskRoute = (markOwnTaskAsDone) => {
  const tasksRoute = express.Router();

  tasksRoute.post("/", async (req, res, next) => {
    console.log("post markOwnTaskAsDone");
    try {
      const done = await markOwnTaskAsDone(req.user, req.body.title);
      console.log(done);
      res
        .status(200)
        .json({ message: `A user task was successfully marked as "done"` });
    } catch (err) {
      // err.status = 404;a
      next(err);
    }
  });
  return tasksRoute;
};

export const deleteTaskRoute = (deleteTask) => {
  const tasksRoute = express.Router();
  tasksRoute.delete("/", async (req, res, next) => {
    try {
      const { error } = deleteTaskSchema.validate(req.body);
      console.log("delete task");
      console.log(req.originalUrl);
      if (error) {
        return res.status(400).json({ message: `Schema validation failed.` });
      }

      await deleteTask(req.user, req.body);

      res.sendStatus(204);
    } catch (err) {
      console.log(err);
      next(err);
    }
  });
  return tasksRoute;
};
