import express from "express";
import passport from "passport";
import registerRoute from "./userRegister.js";
import userLogin from "./userLogin.js";
import {
  createTaskRoute,
  deleteTaskRoute,
  editTaskRoute,
  getUserTasksDoneRoute,
  getUserTasksRoute,
  markUserTaskRoute,
} from "./tasks.js";

const router = express.Router();

const authenticateJwt = (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      return next(err); // handle error in your error handler middleware
    }

    if (!user) {
      const err = new Error(
        "Authorization validation failed. For instance, a request without a token."
      );
      err.status = 401;
      return next(err);
    }

    req.user = user; // assign user to req.user
    next(); // go to next middleware or route handler
  })(req, res, next);
};

export default (usersService, tasksService) => {
  router.get("/", (req, res) => {
    res.send("Welcome to the homepage");
  });

  router.use("/api/auth/register", registerRoute(usersService.register));
  router.use("/api/auth/login", userLogin(usersService.login));
  router.use(authenticateJwt);
  router.use(
    "/api/auth/tasks",

    createTaskRoute(tasksService.createNewTask)
  );

  router.use(
    "/api/tasks/done",

    markUserTaskRoute(tasksService.markOwnTaskAsDone)
  );
  router.use(
    "/api/tasks/done",

    getUserTasksDoneRoute(tasksService.getUserTasksDone)
  );

  router.use(
    "/api/tasks",

    getUserTasksRoute(tasksService.getUserTasks)
  );
  router.use(
    "/api/tasks",

    deleteTaskRoute(tasksService.deleteUserTask)
  );

  router.use(
    "/api/tasks/",

    editTaskRoute(tasksService.editUserTask)
  );

  return router;
};
