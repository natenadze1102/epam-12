import express from "express";
// import { userSchema } from "../validationSchemas/UserSchema.js";

const registerRoute = express.Router();

export default (userRegister) => {
  registerRoute.post("/", async (req, res, next) => {
    try {
      await userRegister(req.body);
      res.status(200).json({ message: "A user was successfully registered." });
    } catch (err) {
      err.status = 400;

      next(err);
    }
  });
  return registerRoute;
};
