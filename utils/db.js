import mongoose from "mongoose";

const connectToDb = async () => {
  try {
    await mongoose.connect(process.env.DB);
    console.log("Sucessfully connected to MongoDB");
    return mongoose;
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

export default connectToDb;
