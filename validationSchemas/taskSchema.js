import Joi from "joi";

export const taskSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().allow(""), // Allows an empty string
});

export const deleteTaskSchema = Joi.object({
  title: Joi.string(),
});
