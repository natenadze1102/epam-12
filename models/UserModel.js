import mongoose from "mongoose";
const { Schema } = mongoose;
import bcrypt from "bcrypt";
import emailValidator from "email-validator";

// Bcrypt
const saltRounds = 10;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    validate: {
      validator: (v) => {
        return emailValidator.validate(v);
      },
      message: (props) =>
        `Email field contains an invalid email (${props.value})`,
    },
    cast: [null, (value, path, model, kind) => `"${value}" is not a string`],
    unique: true,
  },
  password: {
    type: String,

    required: true,
    minlength: [6],
    validate: {
      validator: (v) => {
        return v && v.length >= 6;
      },
      message: (props) =>
        `Password length is less than 6 characters. Got ${props.value.length}`,
    },
    cast: [null, (value, path, model, kind) => `"${value}" is not a string`],
  },
  firstName: String,
  lastName: String,
});

userSchema.pre("save", function (next) {
  const user = this;
  if (!user.isModified("password")) {
    return next();
  }

  bcrypt.hash(user.password, saltRounds, function (err, hash) {
    if (err) {
      next(err);
    }
    user.password = hash;
    next();
  });
});

userSchema.post("save", (error, doc, next) => {
  if (error.name === "MongoServerError" && error.code === 11000) {
    const newError = new Error("User with this email already exists");
    newError.status = 400;
    next(newError);
  } else {
    next();
  }
});

const User = mongoose.model("users", userSchema);

export default User;
