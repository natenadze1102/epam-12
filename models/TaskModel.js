import mongoose from "mongoose";
const { Schema } = mongoose;
import emailValidator from "email-validator";

const taskSchema = new Schema({
  title: {
    type: String,
    required: true,
    cast: [null, (value, path, model, kind) => `"${value}" is not a string`],
  },
  description: {
    type: String,
    cast: [null, (value, path, model, kind) => `"${value}" is not a string`],
  },
  done: {
    type: Boolean,
    cast: [null, (value, path, model, kind) => `"${value}" is not a boolean`],
  },
  email: {
    type: String,
    required: true,
    validate: {
      validator: (v) => {
        return emailValidator.validate(v);
      },
      message: (props) =>
        `Email field contains an invalid email (${props.value})`,
    },
    cast: [null, (value, path, model, kind) => `"${value}" is not a string`],
  },
});

taskSchema.index({ title: 1, email: 1 }, { unique: true });

taskSchema.post("save", (error, doc, next) => {
  checkIndexes();
  if (error.name === "MongoServerError" && error.code === 11000) {
    const newError = new Error(
      "Schema validation of the body failed, or a user already has a task with the provided title."
    );
    newError.status = 400;
    next(newError);
  } else {
    next();
  }
  next();
});

async function checkIndexes() {
  try {
    // taskSchema.clearIndexes();
    const indexes = await Tasks.collection.getIndexes();
  } catch (err) {
    console.error(err);
  }
}

const Tasks = mongoose.model("tasks", taskSchema);

export default Tasks;
