import Tasks from "../models/TaskModel.js";

class TasksService {
  createNewTask = async (loggedUser, tsk) => {
    try {
      const newTask = {
        email: loggedUser.email,
        title: tsk.title,
        description: tsk.description,
        done: false,
      };
      const task = new Tasks(newTask);
      await task.save();
    } catch (error) {
      throw error;
    }
  };
  getTasks = async () => {
    try {
      const tasks = await Tasks.find({}).exec();
      return tasks;
    } catch (error) {
      throw new Error(error);
    }
  };

  getUserTasks = async (loggedUser) => {
    try {
      const foundTasks = await Tasks.find(
        { email: loggedUser.email },
        {
          title: 1,
          description: 1,
          done: 1,
        }
      ).exec();
      return foundTasks;
    } catch (error) {
      throw new Error(error);
    }
  };

  getUserTasksDone = async (loggedUser) => {
    const tasks = await this.getTasks(loggedUser);
    console.log({ tasks, loggedUser });
    return tasks.filter((task) => task.done === true);
  };

  markOwnTaskAsDone = async (loggedUser, queryTitle) => {
    try {
      const res = await Tasks.updateOne(
        { title: queryTitle, email: loggedUser.email },
        { $set: { done: true } }
      );

      if (!res.matchedCount) {
        throw new Error(
          `A task with the provided title ${title} does not exist in the user's to-do list.`
        );
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  validateTask = async (tasks, task) => {
    try {
      const foundTask = tasks.find((existingTask) => {
        return (
          task.email === existingTask.email && task.title === existingTask.title
        );
      });

      if (foundTask) {
        throw new Error(
          "Schema validation of the body failed, or a user already has a task with the provided title."
        );
      }
    } catch (error) {
      throw error;
    }
  };

  editUserTask = async (loggedUser, incommingTask, queryTitle) => {
    try {
      const valuesToUpdate = {
        email: loggedUser.email,
        title: incommingTask.title,
        description: incommingTask.description,
        done: false,
      };

      const res = await Tasks.updateOne(
        { title: queryTitle, email: loggedUser.email },
        { $set: valuesToUpdate }
      );

      if (!res.matchedCount) {
        throw new Error(
          `A task with the provided title ${queryTitle} does not exist in the user's tasks.`
        );
      }
    } catch (error) {
      throw error;
    }
  };

  deleteUserTask = async (loggedUser, tsk) => {
    try {
      await Tasks.deleteOne({ title: tsk.title, email: loggedUser.email });
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  };
}

export default TasksService;
