import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import User from "../models/UserModel.js";

class UsersService {
  // register
  register = async (user) => {
    console.log({ user });
    try {
      const newUser = new User(user);
      await newUser.save();
    } catch (error) {
      console.log(error);
      throw error;
    }
  };
  // login
  login = async (email, password) => {
    try {
      const user = await this.getUserByEmail(email);

      if (user && (await bcrypt.compare(password, user.password))) {
        const token = this.generateJwt(user); // Generate the JWT token
        return { user, token }; // Return the user and token
      } else {
        throw new Error(
          "Email field contains an invalid email, or any of the fields is missing, or a user with the provided credentials does not exist."
        );
      }
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  };

  getUserByEmail = async (email) => {
    const user = await User.findOne({ email: email }).exec();
    return user;
  };

  generateJwt = (user) => {
    const payload = {
      email: user.email,
    };
    const secret = process.env.JWT_SECRET;
    const options = {
      expiresIn: "1h",
    };

    return jwt.sign(payload, secret, options);
  };
}

export default UsersService;
